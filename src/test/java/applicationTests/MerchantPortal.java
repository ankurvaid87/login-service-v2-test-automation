package applicationTests;

import common.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginHomePage;

import java.io.IOException;
import java.lang.reflect.Method;


public class MerchantPortal extends BaseTest{
    private static String userEmail;
    private static String userPassword;
    private final Logger logger = LogManager.getLogger(MerchantPortal.class);

    @BeforeTest
    public void setupUserCreds() throws IOException {
        userEmail = Utils.getProperty("MerchantPortalUserEmail");
        logger.debug("Merchant Portal User Email : " + userEmail);
        userPassword = Utils.getProperty("MerchantPortalUserPassword");
        logger.debug("Merchant Portal User Password : " + userPassword);
    }

    @BeforeMethod
    public void beforeMethod(Method method) throws Exception {
        logger.info("------------------- Starting Test " + method.getName() + " -----------------------------");
        launchBrowserAndGoToHomePage();
    }

    @AfterMethod
    public void afterMethod(ITestResult result){
        if(result.isSuccess()){
            logger.info("------------------- Test Passed -----------------------------");
        }else{
            logger.info("------------------- Test Failed -----------------------------");
        }
        closeBrowser();
    }

    @Test(priority = 0, enabled = false)
    public void verifyMerchantPortalBrandedLoginPage(){
        LoginHomePage.verifyBrandedLoginHomePage();
    }


}
