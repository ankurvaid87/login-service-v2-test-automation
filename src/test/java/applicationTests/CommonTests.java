package applicationTests;

import common.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class CommonTests extends BaseTest{
    private static String userEmail;
    private static String userPassword;
    private String clientConnectSignedInHomeUrl;
    private final Logger logger = LogManager.getLogger(CommonTests.class);

    @BeforeTest
    public void setupUserCreds() throws IOException {
        userEmail = Utils.getProperty("ClientConnectUserEmail");
        logger.debug("Client Connect User Email : " + userEmail);
        userPassword = Utils.getProperty("ClientConnectUserPassword");
        logger.debug("Client Connect User Password : " + userPassword);
        clientConnectSignedInHomeUrl = Utils.getProperty("ClientConnectSignedInHomeUrl");
    }

    @BeforeMethod
    public void beforeMethod(Method method) throws Exception {
        logger.info("------------------- Starting Test " + method.getName() + " -----------------------------");
        launchBrowserAndGoToHomePage();
    }

    @AfterMethod
    public void afterMethod(ITestResult result) throws IOException {
        if(result.isSuccess()){
            logger.info("------------------- Test Passed -----------------------------\n");
            logger.info("-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----\n");
        }else{
            LocalDateTime currentDateTime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
            String formattedDateTime = currentDateTime.format(formatter).replaceAll(":", ".");

            File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            String screenshotFileName = System.getProperty("user.dir") + "/src/main/resources/Screenshot" + formattedDateTime + ".png";
            logger.info("Screenshot saved at " + screenshotFileName);
            FileUtils.copyFile(screenshot, new File(screenshotFileName));

            logger.info("------------------- Test Failed -----------------------------\n");
            logger.info("-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----x-----\n");
            }
        closeBrowser();
    }

    @Test(priority = 0, enabled = true)
    public void verifyClientConnectBrandedLoginPage(){
        LoginHomePage.verifyBrandedLoginHomePage();
    }

    @Test(priority = 1, enabled = true, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void signInWithValidCredentialsAndSignOut() throws InterruptedException, IOException {
        LoginHomePage.enterUserEmail(userEmail);
        LoginHomePage.enterUserPassword(userPassword);
        LoginHomePage.clickOnSignInButton();

        ClientConnectSignedInPage.verifySignedInPage();
        Assert.assertTrue(driver.getCurrentUrl().equals(clientConnectSignedInHomeUrl),
                "Client Connect Signed-In page url does not match expected");

        ClientConnectSignedInPage.signOut();
        LoginHomePage.verifyBrandedLoginHomePage();
    }

    @Test(priority = 2, enabled = true, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void forgotPassword() throws Exception {
        LoginHomePage.clickOnForgotPasswordLink();

        ForgotPasswordPage.verifyForgotPasswordPage();
        ForgotPasswordPage.enterUserEmail(userEmail);
        ForgotPasswordPage.clickOnSendResetLink();

        ForgotPasswordPage.verifyEmailForgotPassword(userEmail);

        ResetPasswordPage.verifyResetPasswordPage();
        ResetPasswordPage.resetPasswordAndReturnToLogin(userPassword);

        LoginHomePage.verifySuccessfulPasswordResetAlert();
    }

    @Test(priority = 3, enabled = false)
    public void claimAccount() throws Exception {
        LoginHomePage.goToClaimAccountPage();

        ClaimAccountPage.verifyClaimAccountPage();
        ClaimAccountPage.enterUserEmailForClaimAccount(userEmail);
        ClaimAccountPage.clickOnResendInvitationButton();

        ClaimAccountPage.verifyEmailClaimAccount(userEmail);
        ClaimAccountPage.verifyCreatePasswordPage(userEmail);

        ClaimAccountPage.createPasswordAndReturnToLogin(userPassword);
        LoginHomePage.verifyBrandedLoginHomePage();
    }

    @Test(priority = 4, enabled = false, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void loginWithIncorrectPassword(){
        LoginHomePage.enterUserEmail(userEmail);
        LoginHomePage.enterUserPassword("Invalid Password");
        LoginHomePage.clickOnSignInButton();
        LoginHomePage.verifyIncorrectPwdErrorMsg();
    }

    @Test(priority = 5, enabled = false, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void loginWithoutCredentials(){
        LoginHomePage.clickOnSignInButton();
        LoginHomePage.verifyBlankFieldValidation();
    }

    @Test(priority = 6, enabled = false, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void userEmailFieldValidation(){
        LoginHomePage.enterUserEmail("InvalidEmail");
        LoginHomePage.enterUserPassword("InvalidPassword");
        LoginHomePage.verifyInvalidEmailFieldValidation();
    }

    @Test(priority = 7, enabled = false, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void forgotPasswordEmailValidation(){
        LoginHomePage.clickOnForgotPasswordLink();
        ForgotPasswordPage.verifyForgotPasswordPage();

        ForgotPasswordPage.checkEmailValidation("InvalidEmail");
    }

    @Test(priority = 8, enabled = false)
    public void claimAccountEmailValidation(){
        LoginHomePage.goToClaimAccountPage();
        ClaimAccountPage.verifyClaimAccountPage();

        ClaimAccountPage.checkEmailValidation("InvalidEmail");
    }

    @Test(priority = 9, enabled = true, dependsOnMethods = {"verifyClientConnectBrandedLoginPage"})
    public void invalidTokenForgotPassword() throws Exception {
        LoginHomePage.clickOnForgotPasswordLink();

        ForgotPasswordPage.verifyForgotPasswordPage();
        ForgotPasswordPage.enterUserEmail(userEmail);
        ForgotPasswordPage.clickOnSendResetLink();
        ForgotPasswordPage.verifyEmailForgotPassword(userEmail);
        ResetPasswordPage.verifyResetPasswordPage();

        ResetPasswordPage.changeTokenInUrl();
        ResetPasswordPage.validateIncorrectTokenMsg();
    }

    @Test(priority = 10, enabled = false)
    public void invalidTokenClaimAccount() throws InterruptedException {
        LoginHomePage.goToClaimAccountPage();

        ClaimAccountPage.verifyClaimAccountPage();
        ClaimAccountPage.enterUserEmailForClaimAccount(userEmail);
        ClaimAccountPage.clickOnResendInvitationButton();
        ClaimAccountPage.verifyEmailClaimAccount(userEmail);
        ClaimAccountPage.verifyCreatePasswordPage(userEmail);

        ClaimAccountPage.changeTokenInUrl();
        ClaimAccountPage.verifyClaimAccountPage();
    }
}
