package applicationTests;

import common.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import pages.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;
    protected Properties properties;
    protected String env;
    protected String appName;
    protected Utils utils;
    protected BasePage basePage;
    private final Logger logger = LogManager.getLogger(BaseTest.class);
    private String browserName;
    private String browserType;

    @BeforeTest
    @Parameters({"browserName", "browserType", "env", "appName"})
    public void setup(String browserName, String browserType, String env, String appName) throws Exception {
        logger.debug("Before Test BaseTest");
        this.env = env;
        this.appName = appName;
        LoginHomePage.setAppName(appName);
        this.browserName = browserName;
        this.browserType=  browserType;
        utils = new Utils(appName, env);
        logger.info("Browser Name : " + browserName + ", " + "Browser Type : " + browserType + ", " + "Environment : "
                + env + ", " + "Application Name : " + appName);
    }

    /**
     * Launches browser based on browserType and browserName passed as arguments
     * TODO: Implement switch case and remove if else
     */
    public void launchBrowserAndGoToHomePage() throws Exception {
        if(browserName.equalsIgnoreCase("chrome") && browserType.equalsIgnoreCase("normal")){
            launchNormalChromeBrowser();
        }else if(browserName.equalsIgnoreCase("chrome") && browserType.equalsIgnoreCase("headless")){
            launchHeadlessChromeBrowser();
        }else if(browserName.equalsIgnoreCase("firefox") && browserType.equalsIgnoreCase("normal")){
            launchNormalFirefoxBrowser();
        }else if(browserName.equalsIgnoreCase("firefox") && browserType.equalsIgnoreCase("headless")){
            launchHeadlessFirefoxBrowser();
        }else{
            launchHeadlessChromeBrowser();
        }

        BasePage.initDriver(driver);
        System.out.println(Utils.getAppUrl());
        driver.get(Utils.getAppUrl());
        Thread.sleep(5000);
        if(env.equalsIgnoreCase("dev")){
            String devURL = driver.getCurrentUrl().replace("login2-pre", "login2-dev");
            driver.get(devURL);
        }
    }

    /**
     * Launches Headless Chrome Browser
     */
    public void launchHeadlessChromeBrowser() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--headless", "--disable-gpu", "--start-maximized", "--window-size=1920,1080", "--ignore-certificate-errors",
//                "--disable-extensions","--no-sandbox","--disable-dev-shm-usage");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--window-size=1280,800");
        options.addArguments("--allow-insecure-localhost");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--ignore-certificate-errors");
        if(System.getProperty("os.name").contains("Windows")){
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
        }else{
            driver = new ChromeDriver(options);
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    /**
     * Launches Headless Firefox Browser
     */
    public void launchHeadlessFirefoxBrowser() throws MalformedURLException {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless", "--disable-gpu", "start-maximized", "--window-size=1920,1080", "--ignore-certificate-errors",
                "--disable-extensions","--no-sandbox","--disable-dev-shm-usage");
        if(System.getProperty("os.name").contains("Windows")){
            driver = new RemoteWebDriver(new URL("http://localhost:4446/wd/hub"), options);
        }else{
            driver = new FirefoxDriver(options);
        }
    }

    /**
     * Launches Chrome Browser
     */
    public void launchNormalChromeBrowser() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-gpu", "--start-maximized", "--ignore-certificate-errors",
                "--disable-extensions","--no-sandbox");
        if(System.getProperty("os.name").contains("Windows")){
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), options);
        }else{
            driver = new ChromeDriver(options);
        }
    }

    /**
     * Launches Firefox Browser
     */
    public void launchNormalFirefoxBrowser() throws MalformedURLException {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--disable-gpu", "--start-maximized", "--ignore-certificate-errors",
                "--disable-extensions","--no-sandbox","--disable-dev-shm-usage");
        if(System.getProperty("os.name").contains("Windows")){
            driver = new RemoteWebDriver(new URL("http://localhost:4446/wd/hub"), options);
        }else{
            driver = new FirefoxDriver(options);
        }
    }

    /**
     * Closes the Browser
     */
    public void closeBrowser(){
        driver.quit();
    }
}
