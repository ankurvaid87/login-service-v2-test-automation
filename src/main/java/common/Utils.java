package common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    private static Properties properties;
    private static String appName;
    private static String env;

    public Utils(String appName, String env){
        Utils.appName = appName;
        Utils.env = env;
    }

    /**
     * Reads property file based on env and returns property value (String)
     * @param propertyName
     * @return
     * @throws IOException
     */
    public static String getProperty(String propertyName) throws IOException {
        FileInputStream fis = new FileInputStream("src/test/resources/properties/" + env + ".properties");
        Properties properties = new Properties();
        properties.load(fis);

        return properties.get(propertyName).toString();
    }

    /**
     * Gets Application url from properties file
     * @return
     */
    public static String getAppUrl() throws Exception {
        try{
            return getProperty(appName);
        }catch(Exception e){
            throw new Exception("Could not find any property with name " + appName + " in " + env + ".properties file");
        }
    }

    /**
     * Gets the user's email from properties file
     * @return
     * @throws Exception
     */
    public static String getUserEmail() throws Exception {
        String propertyName = "";
        try{
            propertyName = appName + "UserEmail";
            return getProperty(propertyName);
        }catch(Exception e){
            throw new Exception("Could not find any property with name " + propertyName + " in " + env + ".properties file");
        }
    }

    /**
     * Gets the user's password from properties file
     * @return
     * @throws Exception
     */
    public static String getUserPassword() throws Exception {
        String propertyName = "";
        try{
            propertyName = appName + "UserPassword";
            return getProperty(propertyName);
        }catch(Exception e){
            throw new Exception("Could not find any property with name " + propertyName + " in " + env + ".properties file");
        }
    }
}
