package pages;

import common.TestData;
import common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;

public class ClientConnectSignedInPage extends BasePage{

    private static final String userMenu = "#user-menu";
    private static final String signedInUserName = "//h2[@class='text-center text-white mt-0 mb-5']/span";
    private static final String clientConnectBrandTitle = "#product-name";
    private static final String signOutLink = "#user-sign-out";

    /**
     * Verifies Signed In Page for SendPro
     */
    public static void verifySignedInPage() throws InterruptedException, IOException {
        waitForElementToBeVisible(By.cssSelector(clientConnectBrandTitle), 15);
        String brandTitle = findElementByCss(clientConnectBrandTitle).getText().trim();
        Assert.assertTrue(brandTitle.matches("Client Connect"), "Signed In page brand title does not match expected");

//        waitForElementToBeVisible(By.cssSelector(userMenu), 10);
//        findElementByCss(userMenu).click();
        Thread.sleep(2000);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", findElementByCss(userMenu));
        Thread.sleep(2000);
        waitForElementToBeVisible(By.xpath(signedInUserName), 5);
        String userName = findElementByXpath(signedInUserName).getText().split(" ")[1];
        Assert.assertTrue(userName.equalsIgnoreCase(Utils.getProperty("ClientConnectSignedInUserName")));
    }

    /**
     * Signs out current user from signed in page
     */
    public static void signOut() throws InterruptedException {
//        waitForElementToBeVisible(By.cssSelector(signOutLink), 2);
//        findElementByCss(signOutLink).click();
        Thread.sleep(2000);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].click();", findElementByCss(signOutLink));
    }
}
