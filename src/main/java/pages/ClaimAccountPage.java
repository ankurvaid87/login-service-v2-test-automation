package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import java.util.ArrayList;

public class ClaimAccountPage extends BasePage{

    private static final String completeYourRegistrationText = "div.signin-box>h2";
    private static final String infoForUser = "div.signin-box>p";
    private static final String textBox = "#username";
    private static final String resendInvitationButton = "#reclaimAccountButton";
    private static final String infoForUserExpectedText = "Your previous invitation to register is no longer valid." +
            " Enter the same email you use for Pitney Bowes and we'll send you a new one.";
    private static final String usernameMsg1 = "div.signin-wrapper>div.signin-box>div:nth-of-type(1)";
    private static final String userEmailText = "div.signin-wrapper>div.signin-box>div:nth-of-type(2)";
    private static final String usernameMsg1Text = "Your Pitney Bowes username is";
    private static final String usernameMsg2 = "div.signin-wrapper>div.signin-box>div:nth-of-type(3)";
    private static final String usernameMsg2Text = "Create your password below";
    private static final String passwordInstruction1 = ".alert.alert-info>p";
    private static final String passwordInstruction2 = ".alert.alert-info>ul>li:nth-of-type(1)>span";
    private static final String passwordInstruction3 = ".alert.alert-info>ul>li:nth-of-type(2)>span";
    private static final String passwordInstruction4 = ".alert.alert-info>ul>li:nth-of-type(3)>span";
    private static final String newPasswordTextBox = "#newPassword";
    private static final String confirmPasswordTextBox = "#confirmPassword";
    private static final String completeMyRegButton = "#claimAccountButton";
    private static final String emailValidationMsg = "#username+div";
    private static final String mailinatorEmailTextBox = "#search";
    private static final String resetPasswordEmail = "//td[contains(text(), 'Reset your password')]";
    private static final String claimAccountEmail = "//td[contains(text(), 'Complete your registration')]";
    private static final String resetLink = "//p[@class = 'button-row']/a[contains(text(), 'Reset')]";
    private static final String completeRegistrationLink = "//p[@class = 'button-row']/a[contains(text(), 'Complete Registration')]";
    private static final String claimAccountEmailSentText = "div.signin-box>p";
    private static final String incorrectTokenMsg = ".alert.alert-danger.mt-3>div>div>span";

    private static final Logger logger = LogManager.getLogger(ClaimAccountPage.class);

    /**
     * Types the user email in provided text box
     * @param userEmail
     */
    public static void enterUserEmailForClaimAccount(String userEmail){
        findElementByCss(textBox).sendKeys(userEmail);
    }

    /**
     * Verifies all elements present on claim account page
     */
    public static void verifyClaimAccountPage(){
        waitForElementToBeVisible(By.cssSelector(completeYourRegistrationText), 15);
        String completeRegTextActual = findElementByCss(completeYourRegistrationText).getText();
        Assert.assertTrue(completeRegTextActual.equals("Complete your registration"),
                "Complete your registration text doesn't match the expected text to be displayed on claim account page");

        waitForElementToBeVisible(By.cssSelector(infoForUser), 10);
        String infoForUserActualText = findElementByCss(infoForUser).getText();
        Assert.assertTrue(infoForUserActualText.equals(infoForUserExpectedText), "Info for user doesn't match the expected text");

        findElementByCss(resendInvitationButton);
        findElementByCss(textBox);
    }

    /**
     * Clicks on Resend Invitation button
     */
    public static void clickOnResendInvitationButton(){
        waitForElementToBeVisible(By.cssSelector(resendInvitationButton), 10);
        findElementByCss(resendInvitationButton).click();
    }

    /**
     * Verifies all elements of create password page
     * @param userEmail
     */
    public static void verifyCreatePasswordPage(String userEmail){
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        waitForElementToBeVisible(By.cssSelector(usernameMsg1), 15);

        Assert.assertTrue(findElementByCss(usernameMsg1).getText().equals(usernameMsg1Text),
                "Welcome message on claim account page does not match expected message");
        Assert.assertTrue(findElementByCss(userEmailText).getText().trim().equals(userEmail),
                "Welcome message (user email) on claim account page does not match expected message");
        Assert.assertTrue(findElementByCss(usernameMsg2).getText().equals(usernameMsg2Text),
                "Welcome message on claim account page does not match expected message");

        Assert.assertTrue(findElementByCss(passwordInstruction1).getText().equals("Your password must include at least:"),
                "Password creation instruction message does not match the expected message");
        Assert.assertTrue(findElementByCss(passwordInstruction2).getText().equals("8 characters"),
                "Password creation instruction message does not match the expected message");
        Assert.assertTrue(findElementByCss(passwordInstruction3).getText().equals("1 uppercase letter"),
                "Password creation instruction message does not match the expected message");
        Assert.assertTrue(findElementByCss(passwordInstruction4).getText().equals("1 digit or special character"),
                "Password creation instruction message does not match the expected message");
    }

    /**
     * Creates new password on claim account page
     * @param userPassword
     */
    public static void createPasswordAndReturnToLogin(String userPassword) throws Exception {
        waitForElementToBeVisible(By.cssSelector(newPasswordTextBox), 10);
        findElementByCss(newPasswordTextBox).sendKeys(userPassword);
        findElementByCss(confirmPasswordTextBox).sendKeys(userPassword);
        findElementByCss(completeMyRegButton).click();
    }

    public static void checkEmailValidation(String email){
        clickOnResendInvitationButton();

        waitForElementToBeVisible(By.cssSelector(emailValidationMsg), 2);
        Assert.assertTrue(findElementByCss(emailValidationMsg).getText().equals("Email is required."),
                "Validation message displayed for blank email does not match the expected message");

        enterUserEmailForClaimAccount(email);
        Assert.assertTrue(findElementByCss(emailValidationMsg).getText().equals("Enter a valid email address."),
                "Validation message displayed for invalid email does not match the expected message");
    }

    /**
     * Verifies email from mailinator account and clicks on Complete Registration Link in the received email
     */
    public static void verifyEmailClaimAccount(String userEmail) throws InterruptedException {
        Thread.sleep(3000);
        waitForElementToBeVisible(By.cssSelector(claimAccountEmailSentText), 5);
        Assert.assertTrue(findElementByCss(claimAccountEmailSentText).getText().equals("We've emailed you a link to set up your account."),
                "Claim account email sent message does not match expected message");

        driver.get("https://mailinator.com");
        waitForElementToBeVisible(By.cssSelector(mailinatorEmailTextBox), 15);
        findElementByCss(mailinatorEmailTextBox).sendKeys(userEmail);
        findElementByCss(mailinatorEmailTextBox).sendKeys(Keys.RETURN);
        waitForElementToBeVisible(By.xpath(claimAccountEmail), 5);
        findElementByXpath(claimAccountEmail).click();
        driver.switchTo().frame("html_msg_body");
        waitForElementToBeVisible(By.xpath(completeRegistrationLink), 5);
        findElementByXpath(completeRegistrationLink).click();
    }

    /**
     * Changes the reset password token in the current url
     */
    public static void changeTokenInUrl() throws InterruptedException {
        Thread.sleep(5000);
        String resetPwdPageUrl = driver.getCurrentUrl();
        logger.debug("Reset Password Page URL before changing token : \n" + resetPwdPageUrl);
        String resetPwdToken = resetPwdPageUrl.split("&productId=")[0].split("\\?token=")[1];
        logger.debug("Reset Password Token : " + resetPwdToken);
        String newToken = resetPwdToken + "pW8d7";
        logger.debug("New Reset Password Token : " + newToken);
        String newUrl = resetPwdPageUrl.replace(resetPwdToken, newToken);
        logger.debug("New URL for Password Reset : \n" + newUrl);
        driver.get(newUrl);
    }
}
