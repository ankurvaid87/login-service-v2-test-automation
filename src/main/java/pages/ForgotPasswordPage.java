package pages;

import common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.util.Properties;

public class ForgotPasswordPage extends BasePage{
    private Properties properties;

    private static final String forgotPasswordHeading = ".signin-box>div>h2";
    private static final String forgotPasswordDescription = ".signin-box>div>p";
    private static final String sendResetLinkButton = "#forgotPasswordButton";
    private static final String returnToSignInLink = "//a[text() = 'Return to Sign In']";
    private static final String emailTextBox = "#username";
    private static final String resetLinkSentmessage = ".signin-box>p";
    private static final String emailValidationMsg = "#username+div";
    private static final String mailinatorEmailTextBox = "#search";
    private static final String resetPasswordEmail = "//td[contains(text(), 'Reset your password')]";
    private static final String claimAccountEmail = "//td[contains(text(), 'Complete your registration')]";
    private static final String resetLink = "//p[@class = 'button-row']/a[contains(text(), 'Reset')]";
    private static final String completeRegistrationLink = "//p[@class = 'button-row']/a[contains(text(), 'Complete Registration')]";
    private static final String claimAccountEmailSentText = "div.signin-box>p";

    /**
     * Verifies all elements on Forgot Password Page
     */
    public static void verifyForgotPasswordPage(){
        waitForElementToBeVisible(By.cssSelector(forgotPasswordHeading), 5);
        String forgotPasswordText = findElementByCss(forgotPasswordHeading).getText();
        Assert.assertTrue(forgotPasswordText.equals("Forgot Password?"), "Forgot Password text/heading does not match expected");

        waitForElementToBeVisible(By.cssSelector(forgotPasswordDescription), 2);
        String forgotPasswordDesc = findElementByCss(forgotPasswordDescription).getText();
        Assert.assertTrue(forgotPasswordDesc.matches("If you.ve forgotten your password, it.s easy to reset it. Just enter the email address for your account."),
                "Forgot Password description does not match the expected");

        String resetLinkButtonText = findElementByCss(sendResetLinkButton).getText();
        Assert.assertTrue(resetLinkButtonText.equals("Send Reset Link"), "Reset link button text does not match expected");

        String returnToSignInLinkText = findElementByXpath(returnToSignInLink).getText();
        Assert.assertTrue(returnToSignInLinkText.equals("Return to Sign In"), "Return to Sign In link text does not match expected");
    }

    /**
     * Enters users email in email textbox
     * @param userEmail
     */
    public static void enterUserEmail(String userEmail){
        waitForElementToBeVisible(By.cssSelector(emailTextBox), 5);
        findElementByCss(emailTextBox).sendKeys(userEmail);
    }

    /**
     * Clicks on send reset link button
     */
    public static void clickOnSendResetLink(){
        waitForElementToBeVisible(By.cssSelector(sendResetLinkButton), 2);
        findElementByCss(sendResetLinkButton).click();
    }

    /**
     * Clicks on Return to sign in link
     */
    public static void clickOnReturnToSignInLink(){
        waitForElementToBeVisible(By.cssSelector(returnToSignInLink), 2);
        findElementByCss(returnToSignInLink).click();
    }

    /**
     * Verifies password reset link sent successfully
     */
    public static void verifyResetLinkSent(){
        waitForElementToBeVisible(By.cssSelector(resetLinkSentmessage), 5);
        String linkSentMessage = findElementByCss(resetLinkSentmessage).getText();
        Assert.assertTrue(linkSentMessage.equals("We've emailed you a link to reset your password."), "Reset link sent message does not match expected");
    }

    public static void checkEmailValidation(String userEmail){
        clickOnSendResetLink();

        waitForElementToBeVisible(By.cssSelector(emailValidationMsg), 2);
        Assert.assertTrue(findElementByCss(emailValidationMsg).getText().equals("Email is required."),
                "Validation message displayed for blank email does not match the expected message");

        enterUserEmail(userEmail);
        Assert.assertTrue(findElementByCss(emailValidationMsg).getText().equals("Enter a valid email address."),
                "Validation message displayed for invalid email does not match the expected message");
    }

    /**
     * Verifies email from mailinator account and clicks on Reset Link in the received email
     */
    public static void verifyEmailForgotPassword(String userEmail) throws Exception {
        driver.get("https://mailinator.com");
        waitForElementToBeVisible(By.cssSelector(mailinatorEmailTextBox), 15);
        findElementByCss(mailinatorEmailTextBox).sendKeys(userEmail);
        findElementByCss(mailinatorEmailTextBox).sendKeys(Keys.RETURN);
        waitForElementToBeVisible(By.xpath(resetPasswordEmail), 5);
        findElementByXpath(resetPasswordEmail).click();
        driver.switchTo().frame("html_msg_body");
        waitForElementToBeVisible(By.xpath(resetLink), 5);
        findElementByXpath(resetLink).click();
    }
}
