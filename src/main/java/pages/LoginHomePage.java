package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginHomePage extends BasePage{
    private static String appName;
    private final JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
    private static final String brandTitle = "h1.signin-title.m-0";
    private static final String pitneyBowesLogo = "title";
    private static final String emailTextboxLabel = "div.form-group>label[for='username']";
    private static final String emailTextbox = "#username";
    private static final String passwordTextboxLabel = "div.form-group>label[for='password']";
    private static final String passwordTextbox = "#password";
    private static final String signInButtonLabel = "#signinButton>span";
    private static final String forgotPasswordLink = "#forgot-password-link";
    private static final String needHelpLink = "div.text-center>a";
    private static final String localeChangeDropdown = "#localeChangeDropdown";
    private static final String localeChangeLanguageList = "#localeChangeDropdown>option";
    private static final String copyrightTextFooter = ".d-block.d-sm-inline-block.text-center.text-sm-left.pt-1.pb-3.p-sm-0";
    private static final String privacyLink = ".d-block.mx-3";
    private static final String termsAndConditionsLink = privacyLink + "+.d-block";
    private static final String passwordResetSuccessfulAlertMessage = "div.alert.alert-info.mt-3>span";
    private static final String incorrectPwdErrorMsg = "div.alert.alert-danger.mt-3>div>div>span";
    private static final String emailFieldValidation = "#username+div";
    private static final String pwdFieldValidation = "div.inner-addon+div";
    private static final List<String> expectedLanguages = new ArrayList<String>();
    private static final Logger logger = LogManager.getLogger(LoginHomePage.class);

    /**
     * Sets the value of appName
     * @param applicationName
     */
    public static void setAppName(String applicationName){
        appName = applicationName;
    }

    /**
     * Checks if all languages in the locale dropdown are in the expected language list
     * @param elementList
     * @param expectedLanguages
     * @return
     */
    public static boolean checkLanguagesLocaleDropdown(List<WebElement> elementList, List<String> expectedLanguages){
        String languages = "Čeština,Dansk,Deutsch,English (Australia),English (Canada),English (Ireland),English (New Zealand),English (United Kingdom)," +
                "English (United States),Español (Estados Unidos),Español (España),Français (Canada),Français (France),Italiano,Nederlands (Nederland)," +
                "Norsk,Polski,Português (Brasil),Русский,Suomi,Svenska,Türkçe";
        String[] arr = languages.split(",");
        expectedLanguages = Arrays.stream(arr).toList();
        for(int i = 0; i <= elementList.size()-4; i++){
            if(!expectedLanguages.contains(elementList.get(i).getText())){
                logger.error(elementList.get(i).getText() + " language displayed in the change locale dropdown is not found in the expected language list");
                return false;
            }
        }
        return true;
    }

    /**
     * Verifies application specific Login Home page
     */
    public static void verifyBrandedLoginHomePage(){
        String needHelpLinkText;
        List<WebElement> languageList;
        switch (appName){
            case "SendPro":
                waitForElementToBeVisible(By.cssSelector(brandTitle), 15);
                String sendProTitle = findElementByCss(brandTitle).getText();
                Assert.assertTrue(sendProTitle.matches("SendPro .Online"), "SendPro Brand title is not displayed OR did not match expected string");
                needHelpLinkText = findElementByCss(needHelpLink).getText();
                Assert.assertTrue(needHelpLinkText.equals("Need Help?"), "Need Help? link is not displayed OR link text does not match expected text");
                Assert.assertTrue(findElementByCss(localeChangeDropdown).isDisplayed(), "Locale change dropdown is not displayed");
                languageList = findElementsByCss(localeChangeLanguageList);
                Assert.assertTrue(checkLanguagesLocaleDropdown(languageList, expectedLanguages), "Locale dropdown language list does not match expected");
                break;

            case "ClientConnect":
                waitForElementToBeVisible(By.cssSelector(brandTitle), 20);
                String clientConnectTitle = findElementByCss(brandTitle).getText();
                Assert.assertTrue(clientConnectTitle.equals("Client Connect"), "Client Connect Brand title is not displayed OR did not match expected string");
                needHelpLinkText = findElementByCss(needHelpLink).getText();
                Assert.assertTrue(needHelpLinkText.equals("Sign up now"), "Sign up now link is not displayed OR link text did not match expected string");
                break;

            case "MerchantPortal":
                waitForElementToBeVisible(By.cssSelector(brandTitle), 15);
                String merchantPortalTitle = findElementByCss(brandTitle).getText();
                Assert.assertTrue(merchantPortalTitle.contains("Merchant Portal"), "Merchant Portal title is not displayed OR did not match expected string");
                Assert.assertTrue(findElementByCss(localeChangeDropdown).isDisplayed(), "Locale change dropdown is not displayed");
//                findElementByCss(localeChangeDropdown).click();
//                waitForElementToBeVisible(By.cssSelector(localeChangeLanguageList), 2);
//                languageList = findElementsByCss(localeChangeLanguageList);
//                Assert.assertTrue(checkLanguagesLocaleDropdown(languageList, expectedLanguages), "Locale dropdown language list does not match expected");
                break;

        }
        verifyHomePageComponents();
    }

    /**
     * Verifies all elements for Login Home page
     */
    public static void verifyHomePageComponents(){
        waitForElementToBeVisible(By.cssSelector(emailTextboxLabel), 5);
        String emailText = findElementByCss(emailTextboxLabel).getText();
        String passwordText = findElementByCss(passwordTextboxLabel).getText();
        String signInButtonText = findElementByCss(signInButtonLabel).getText();
        String forgotPasswordText = findElementByCss(forgotPasswordLink).getText();

        String copyrightText = findElementByCss(copyrightTextFooter).getText();
        String privacyText = findElementByCss(privacyLink).getText();
        String termsAndConditionsText = findElementByCss(termsAndConditionsLink).getText();
        String pageTitle = driver.getTitle();

        Assert.assertTrue(pageTitle.equals("Pitney Bowes"));
        Assert.assertTrue(emailText.equals("Email"));
        Assert.assertTrue(passwordText.equals("Password"));
        Assert.assertTrue(signInButtonText.equals("Sign In"));
        Assert.assertTrue(forgotPasswordText.equals("Forgot your password?"));
        Assert.assertTrue(copyrightText.matches(". 2022 Pitney Bowes Inc\\. All rights reserved\\."), "copyright text mismatch");
        Assert.assertTrue(privacyText.equals("Privacy"));
        Assert.assertTrue(termsAndConditionsText.equals("Terms of Use"));
    }

    /**
     * Enters users email in email textbox
     * @param userEmail
     */
    public static void enterUserEmail(String userEmail){
        waitForElementToBeVisible(By.cssSelector(emailTextbox), 5);
        findElementByCss(emailTextbox).sendKeys(userEmail);
    }

    /**
     * Enters users email in email textbox
     * @param password
     */
    public static void enterUserPassword(String password){
        waitForElementToBeVisible(By.cssSelector(passwordTextbox), 5);
        findElementByCss(passwordTextbox).sendKeys(password);
    }

    /**
     * Clicks on sign in button
     */
    public static void clickOnSignInButton(){
        waitForElementToBeVisible(By.cssSelector(signInButtonLabel), 5);
        findElementByCss(signInButtonLabel).click();
    }

    /**
     * Clicks on forgot password link
     */
    public static void clickOnForgotPasswordLink(){
        waitForElementToBeVisible(By.cssSelector(forgotPasswordLink), 10);
        findElementByCss(forgotPasswordLink).click();
    }

    /**
     * Verifies password reset successful message
     */
    public static void verifySuccessfulPasswordResetAlert(){
        waitForElementToBeVisible(By.cssSelector(passwordResetSuccessfulAlertMessage), 10);
        String pwdResetSuccessMsg = findElementByCss(passwordResetSuccessfulAlertMessage).getText();
        Assert.assertTrue(pwdResetSuccessMsg.equals("Your password has been successfully reset. Use it to sign in below."),
                "Password Reset successful message does not match expected");
    }

    /**
     * Takes user to Claim Account Page
     */
    public static void goToClaimAccountPage(){
        String currentUrl = driver.getCurrentUrl();
        String claimAccountPageUrl = currentUrl.replace("/login?", "/claim-account?");
        driver.get(claimAccountPageUrl);
    }

    /**
     * Verifies the incorrect password error message
     */
    public static void verifyIncorrectPwdErrorMsg(){
        String expectedMsg = "Invalid credentials entered, 6 wrong attempts will result in lockout of account";
        waitForElementToBeVisible(By.cssSelector(incorrectPwdErrorMsg), 5);
        Assert.assertTrue(findElementByCss(incorrectPwdErrorMsg).getText().equals(expectedMsg),
                "The message displayed for incorrect password entered by user does not match expected message");
    }

    /**
     * Verifies the field validations for email and password
     */
    public static void verifyBlankFieldValidation(){
        String expectedMsgEmail = "Email is required.";
        String expectedMsgPwd = "Password is required.";

        waitForElementToBeVisible(By.cssSelector(emailFieldValidation), 5);
        Assert.assertTrue(findElementByCss(emailFieldValidation).getText().equals(expectedMsgEmail),
                "The message displayed for blank email field validation does not match expected message");

        waitForElementToBeVisible(By.cssSelector(pwdFieldValidation), 5);
        Assert.assertTrue(findElementByCss(pwdFieldValidation).getText().equals(expectedMsgPwd),
                "The message displayed for blank password field validation does not match expected message");
    }

    public static void verifyInvalidEmailFieldValidation(){
        String expectedMsgEmail = "Enter a valid email address.";

        waitForElementToBeVisible(By.cssSelector(emailFieldValidation), 5);
        Assert.assertTrue(findElementByCss(emailFieldValidation).getText().equals(expectedMsgEmail),
                "The message displayed for invalid email field validation does not match expected message");
    }
}
