package pages;

import common.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;

public class ResetPasswordPage extends BasePage{

    private static final String newPasswordTextBox = "#newPassword";
    private static final String newPasswordTextBoxLabel = "//label[text() = 'New password']";
    private static final String confirmPasswordTextBox = "#confirmPassword";
    private static final String confirmPasswordTextBoxLabel = "//label[text() = 'Confirm password']";
    private static final String submitButton = "#resetPasswordButton";
    private static final String returnToSignInLink = "//a[text() = 'Return to Sign In']";
    private static final String incorrectTokenMsg = ".alert.alert-danger.mt-3>div>div>span";
    private static final Logger logger = LogManager.getLogger(ResetPasswordPage.class);

    /**
     * Verifies Password Reset Page
     */
    public static void verifyResetPasswordPage() throws InterruptedException, IOException {
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        waitForElementToBeVisible(By.xpath(newPasswordTextBoxLabel), 20);
        String newPasswordText = findElementByXpath(newPasswordTextBoxLabel).getText();
        Assert.assertTrue(newPasswordText.equals("New password"), "Text box label for new password does not say New password");

        String currentUrl = driver.getCurrentUrl();
        String env = Utils.getProperty("env").toString();
        Assert.assertTrue(currentUrl.contains("https://login2-" + env + ".saase2e.pitneycloud.com/reset-password?"),
                "Current page url does not match expected");

        String confirmPasswordText = findElementByXpath(confirmPasswordTextBoxLabel).getText();
        Assert.assertTrue(confirmPasswordText.equals("Confirm password"), "Text box label for confirm password does not say Confirm password");
    }

    /**
     * Resets the user password and clicks on submit button
     * @param newPassword
     * @throws Exception
     */
    public static void resetPasswordAndReturnToLogin(String newPassword) throws Exception {
        findElementByCss(newPasswordTextBox).sendKeys(newPassword);
        findElementByCss(confirmPasswordTextBox).sendKeys(newPassword);
        findElementByCss(submitButton).click();
    }

    /**
     * Changes the reset password token in the current url
     */
    public static void changeTokenInUrl() throws InterruptedException {
        Thread.sleep(5000);
        String resetPwdPageUrl = driver.getCurrentUrl();
        logger.debug("Reset Password Page URL before changing token : \n" + resetPwdPageUrl);
        String resetPwdToken = resetPwdPageUrl.split("&productId=")[0].split("\\?token=")[1];
        logger.debug("Reset Password Token : " + resetPwdToken);
        String newToken = resetPwdToken + "pW8d7";
        logger.debug("New Reset Password Token : " + newToken);
        String newUrl = resetPwdPageUrl.replace(resetPwdToken, newToken);
        logger.debug("New URL for Password Reset : \n" + newUrl);
        driver.get(newUrl);
    }

    /**
     * Validates incorrect token message for reset password
     */
    public static void validateIncorrectTokenMsg() throws InterruptedException {
        waitForElementToBeVisible(By.cssSelector(incorrectTokenMsg), 10);
        logger.debug("Error message for invalid token :" + "\n" + findElementByCss(incorrectTokenMsg).getText());
        Assert.assertTrue(findElementByCss(incorrectTokenMsg).getText().equals(
                "The email link you have tried to use is no longer valid. Please enter your username to send a new link."),
                "Error message for incorrect token does not match expected message");
    }
}
