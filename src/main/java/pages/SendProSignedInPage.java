package pages;

import common.TestData;
import org.openqa.selenium.By;
import org.testng.Assert;

public class SendProSignedInPage extends BasePage{

    private static final String signedInUserName = "#header-user>span";
    private static final String signedInUserNameLink = "#header-user";
    private static final String sendProBrandTitle = "#header-homeIcon>span";
    private static final String signOutLink = "#header-signOut";


    /**
     * Verifies Signed In Page for SendPro
     */
    public static void verifySignedInPage(){
        waitForElementToBeVisible(By.cssSelector(sendProBrandTitle), 10);
        String brandTitle = findElementByCss(sendProBrandTitle).getText();
        Assert.assertTrue(brandTitle.matches("SendPro. Online"), "Signed In page brand title does not match expected");

        String userName = findElementByCss(signedInUserName).getText();
        Assert.assertTrue(userName.equals(TestData.SIGNED_IN_USER_NAME_SEND_PRO));
    }

    /**
     * Signs out current user from signed in page
     */
    public static void signOut(){
        waitForElementToBeVisible(By.cssSelector(signedInUserNameLink), 5);
        findElementByCss(signedInUserNameLink).click();
        waitForElementToBeVisible(By.cssSelector(signOutLink), 2);
        findElementByCss(signOutLink).click();
    }
}
