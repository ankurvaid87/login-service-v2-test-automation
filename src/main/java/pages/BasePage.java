package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage {
    protected static WebDriver driver;
    protected String env;
    private static final Logger logger = LogManager.getLogger(BasePage.class);

    /**
     * Instantiate driver
     * @param remoteDriver
     */
    public static void initDriver(WebDriver remoteDriver){
        driver = remoteDriver;
    }

    /**
     * Returns a WebElement by locating the same with css
     * @param elementLocator
     * @return
     */
    public static WebElement findElementByCss(String elementLocator){
        return driver.findElement(By.cssSelector(elementLocator));
    }

    /**
     * Returns a WebElement by locating the same with xpath
     * @param elementLocator
     * @return
     */
    public static WebElement findElementByXpath(String elementLocator){
        return driver.findElement(By.xpath(elementLocator));
    }

    /**
     * Waits for the element to be visible on the page
     * @param element
     */
    public static void waitForElementToBeVisible(By element, int timeOutInSeconds){
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        try{
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        }catch (TimeoutException t){
            logger.fatal("Element not visible after trying for " + timeOutInSeconds + " secs");
        }
    }

    /**
     * Waits for the element to disappear from the page
     * @param element
     */
    public static void waitForElementToDisappear(By element){
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
    }

    /**
     * Returns a list of WebElements
     * @param elementLocator
     * @return
     */
    public static List<WebElement> findElementsByCss(String elementLocator){
        return driver.findElements(By.cssSelector(elementLocator));
    }
}
