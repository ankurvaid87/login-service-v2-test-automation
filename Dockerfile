# Base image from dockerhub
FROM selenium/standalone-chrome-debug
WORKDIR /login-service-v2-test-automation

COPY ./src /login-service-v2-test-automation
COPY ./.gitignore /login-service-v2-test-automation
COPY ./.gitlab-ci.yml /login-service-v2-test-automation
COPY ./DEVtestng.xml /login-service-v2-test-automation
COPY ./PREtestng.xml /login-service-v2-test-automation
COPY ./pom.xml /login-service-v2-test-automation
COPY ./README.md /login-service-v2-test-automation
COPY ./STGtestng.xml /login-service-v2-test-automation

RUN sudo apt update
RUN sudo apt install maven -y
RUN cd /login-service-v2-test-automation
